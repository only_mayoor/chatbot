//
//  ChatManager.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 21/02/22.
//

import Foundation

class ChatManager {
    
    static let shared = ChatManager()
    
    private init() {
        
    }
    
    func getChatData() -> ChatBotDataModel? {
        
        if let chatData = readFromFile() {
            return chatData
        }
        return nil
    }
    
    /// Loads an array of chat data from local file. Will create one if there is no data
    ///
    /// - Parameters:
    ///   - bot: Will accept Bot model to add in local file.
    ///   - completion: A callback that is called when data saving is done. Will pass chat data object
    
    func addBot(bot:BotModel,completion :(ChatBotDataModel?) -> ()){
        if var chatData = getChatData(), var botData = chatData.data {
            botData.insert(bot, at: 0)
            chatData.data = botData
            let sortedData = sortChatBasedOnTime(chatData: chatData)
            
            saveToFile(data: sortedData) { (isSuccess) in
                if isSuccess {
                    completion(sortedData)
                } else {
                    completion(nil)
                }
            }
        } else {
            var chatDataModel = ChatBotDataModel()
            chatDataModel.data = [bot]
            saveToFile(data: chatDataModel) { (isSuccess) in
                if isSuccess {
                    completion(chatDataModel)
                } else {
                    completion(nil)
                }
            }
        }
    }
    
    /// Will update message data to existing bot data
    ///
    /// - Parameters:
    ///   - message: Message sent by user.
    ///   - index: Index in order to get & update individual bot data form file.
    func sendMessage(message:MessageModel,index:Int,completion :(Bool) -> ()){
        if var chatData = getChatData(), var botData = chatData.data, botData.count > index {
            var selectedChatData = botData[index]
            selectedChatData.modifiedTime = Date()
            selectedChatData.lastMessage = message.message
            
            if var messageData = selectedChatData.messageData {
                messageData.append(message)
                selectedChatData.messageData = messageData
            } else {
                var array = [MessageModel]()
                array.append(message)
                selectedChatData.messageData = array
            }
            
            botData[index] = selectedChatData
            chatData.data = botData
            
            let sortedData = sortChatBasedOnTime(chatData: chatData)
            saveToFile(data: sortedData) { (isSuccess) in
                if isSuccess {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        } else {
            completion(false)
        }
    }
    
    /// Will save chat data to local document directory
    ///
    /// - Parameters:
    ///   - data: Chat Data object.
    ///   - completion: A callback that is called for local document directory storage
    
    fileprivate func saveToFile(data:ChatBotDataModel, completion :(Bool) -> ()){
        
        let jsonEncoder = JSONEncoder()
        
        do {
            let jsonData = try jsonEncoder.encode(data)
            let jsonString = String(data: jsonData, encoding: .utf8)
            
            if let documentDirectory = FileManager.default.urls(for: .documentDirectory,
                                                                   in: .userDomainMask).first {
                let pathWithFilename = documentDirectory.appendingPathComponent("ChatBot.json")
                do {
                    try jsonString?.write(to: pathWithFilename,
                                          atomically: true,
                                          encoding: .utf8)
                    completion(true)
                    
                } catch {
                    completion(false)
                }
            }
        }
        catch {
            completion(false)
        }
    }
    
    /// Will read data from local document directory
    
    fileprivate func readFromFile() -> ChatBotDataModel? {
        
        let fm = FileManager.default
        let urls = fm.urls(for: .documentDirectory, in: .userDomainMask)
        if let url = urls.first {
            var fileURL = url.appendingPathComponent("ChatBot")
            fileURL = fileURL.appendingPathExtension("json")
            do {
                let jsonData = try Data(contentsOf: fileURL)
                let decodedData = try JSONDecoder().decode(ChatBotDataModel.self,from: jsonData)
                return decodedData
                
            } catch {
                return nil
            }
        }
        return nil
    }
    
    
    /// Will sort chat data based on modified time and message count
    
    fileprivate func sortChatBasedOnTime(chatData:ChatBotDataModel) -> ChatBotDataModel {
        var chatModel = ChatBotDataModel()
        if let botData = chatData.data , botData.count > 1 {
            let arrayChat = botData.sorted(by:{$0.modifiedTime ?? Date() > $1.modifiedTime ?? Date()})
            
            var sortedArray = arrayChat.filter { $0.messageData?.count ?? 0 > 0 }
            sortedArray += arrayChat.filter { $0.messageData?.count ?? 0 == 0 }
            chatModel.data = sortedArray
            return chatModel
        }
        return chatData
    }
    
}
