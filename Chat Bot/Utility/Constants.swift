//
//  Constants.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 18/02/22.
//

import UIKit

struct Constants {
    
    static let kDelayReply = 2.0
    
    struct ScreenText {
        static let kAddBotTitle = "Add Bot Friend"
        static let kChatBot = "Chat Bot"
        static let kReplyMessage = "hi hi"
        
    }
    
    struct ErrrorMessage {
        static let kDuplicateBot = "Please enter unique bot name"
        static let kInvalidBotName = "Please enter valid Bot Name"
        static let kError = "Error"
        
    }
    
    
    struct Colors {
        static let appNavTheme = UIColor(red: 0/255.0,
                                         green: 204/255.0,
                                         blue: 204/255.0,
                                         alpha: 1.0)
        static let userMessageTheme = UIColor(red: 223/255.0,
                                              green: 250/255.0,
                                              blue: 211/255.0,
                                              alpha: 1.0)
    }
}
