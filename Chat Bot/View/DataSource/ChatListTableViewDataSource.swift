//
//  ChatListTableViewDataSource.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 18/02/22.

import Foundation
import UIKit


// This generic data source class will help us to implement tableview delegate & datasource methods for bot ChatLList and BotChat page


class ChatListTableViewDataSource<CELL : UITableViewCell,T> : NSObject, UITableViewDataSource,UITableViewDelegate {
    
    private var cellIdentifier : String!
    private var data : [T]!
    var configCell : (CELL, T) -> () = {_,_ in }
    var didSelectElement: (Int) -> () = { _ in }
    
    
    init(identifier : String, items : [T], configureCell : @escaping (CELL, T) -> ()) {
        self.cellIdentifier = identifier
        self.data =  items
        self.configCell = configureCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CELL
        
        let item = self.data[indexPath.row]
        self.configCell(cell, item.self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectElement(indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
