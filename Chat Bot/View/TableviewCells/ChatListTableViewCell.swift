//
//  ChatListTableViewCell.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 18/02/22.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var labelBotName: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imagePlaceholder: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        viewBg.layer.cornerRadius = 10.0

        
    }
    
    
}
