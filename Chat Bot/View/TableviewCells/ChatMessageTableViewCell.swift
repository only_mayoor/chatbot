//
//  ChatMessageTableViewCell.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 21/02/22.
//

import UIKit

class ChatMessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelSenderMessage: UILabel!
    @IBOutlet weak var labelUserMessage: UILabel!
    
    @IBOutlet weak var senderBgView: UIView!
    @IBOutlet weak var userBGView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        senderBgView.layer.cornerRadius = 10
        userBGView.layer.cornerRadius = 10
        userBGView.backgroundColor = Constants.Colors.userMessageTheme
        senderBgView.backgroundColor = UIColor.white

    }
    
    // MARK: - Configure cell baseed on useer type
    func configCell(message:MessageModel) {
        
        if message.userType == UserType.sender.rawValue {
            labelSenderMessage.text = message.message ?? ""
            userBGView.isHidden = true
            senderBgView.isHidden = false

        } else {
            labelUserMessage.text = message.message ?? ""
            senderBgView.isHidden = true
            userBGView.isHidden = false
        }
    }
    
    
}
