//
//  BaseViewController.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 18/02/22.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupView()
    }
    
    // Common function to customise Viewe
    func setupView() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.titleTextAttributes = [.font: UIFont.boldSystemFont(ofSize: 18.0),
                                          .foregroundColor: UIColor.white]
        appearance.backgroundColor = Constants.Colors.appNavTheme
        
        navigationItem.backButtonTitle = ""
        
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        
    }
    
    @objc func actnAddChatPressed(){
        print("Add")
    }
    
}
