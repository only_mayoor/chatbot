//
//  BotChatViewController.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 21/02/22.
//

import UIKit

protocol BotDataProtocol {
    func bindMessageDataToView()
}

protocol BotMessageProtocol {
    func updateChatData()
}

class BotChatViewController: BaseViewController {
    
    // MARK: - IBOutlets / Declarationn
    
    @IBOutlet weak var tableViewChat: UITableView!
    @IBOutlet weak var llabelNoMessage: UILabel!
    @IBOutlet weak var txtChatMessage: UITextView!
    @IBOutlet weak var constraintTextViewBottom: NSLayoutConstraint!
    
    private var chatDataSource: ChatListTableViewDataSource<ChatMessageTableViewCell,MessageModel>!
    
    let kIDentifier = "ChatMessageTableViewCell"
    var viewModel = ChatBotViewModel()
    var selectedIndex:Int?
    var delegate: BotMessageProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegateBot = self
        self.title =  viewModel.getSelectedBotData(index: selectedIndex ?? 0)
    }
    
    static func loadFromNib() -> BotChatViewController {
        let vc = BotChatViewController(nibName: "BotChatViewController", bundle: nil)
        return vc
        
    }
    
    override func setupView() {
        super.setupView()
        tableViewChat.contentInsetAdjustmentBehavior = .never
        tableViewChat.register(UINib(nibName: kIDentifier, bundle: nil), forCellReuseIdentifier: kIDentifier)
        tableViewChat.estimatedRowHeight = 20
        
        txtChatMessage.delegate = self
        txtChatMessage.layer.cornerRadius = 20
        
        /// Adding Keyboard notifications to handle keyboard when user clicks on text view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        /// Will dismiss keyboard when user taps anywhere outside of keyboard.
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
    }
    
    // MARK: - IBActions
    
    @IBAction func actnSendBtnPressed(_ sender: Any) {
        guard let message = txtChatMessage.text, !message.isEmpty else { return }
        viewModel.sendMessage(text: message, user: .user)
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.kDelayReply) {
            self.viewModel.sendMessage(text: Constants.ScreenText.kReplyMessage, user: .sender)
            if let del = self.delegate {
                del.updateChatData()
            }
        }
        txtChatMessage.text = ""
        self.view.endEditing(true)
    }
}

// MARK: - TextView Delegates

extension BotChatViewController: UITextViewDelegate {
    
    /// Will dismiss keyboard on Return button press.
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}


extension BotChatViewController: BotDataProtocol {
    
    // MARK: - Data Binding
    
    /// Will call it from view model to bind our DataSources. It consists closures block for TableView data source and delegate implelmentation. After initializing datasource object we will reload tableview.
    ///
    func bindMessageDataToView() {
        
        self.chatDataSource = ChatListTableViewDataSource(identifier: kIDentifier, items: viewModel.messageData, configureCell: { (cell,message) in
            cell.configCell(message: message)
            cell.selectionStyle = .none
        })
        
        DispatchQueue.main.async {
            self.tableViewChat.dataSource = self.chatDataSource
            self.tableViewChat.delegate = self.chatDataSource
            self.tableViewChat.isHidden = false
            self.llabelNoMessage.isHidden = true
            self.tableViewChat.reloadData()
            
            if !(self.viewModel.messageData.count == 0) {
                self.tableViewChat.scrollToRow(at: IndexPath(item: self.viewModel.messageData.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
    }
}

// MARK: - KeyBoard Notifications

extension BotChatViewController {
    @objc private func keyboardWillShow(notification: NSNotification) {
        
        if let userInfo = notification.userInfo, let keyboardRectangle = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            let window = UIApplication.shared.windows.first
            let bottomPadding = window?.safeAreaInsets.bottom
            
            UIView.animate(withDuration: 0.5) {
                self.tableViewChat.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardRectangle.height - (bottomPadding ?? 0.0) + 110.0 , right: 0)
                self.constraintTextViewBottom.constant = keyboardRectangle.height - (bottomPadding ?? 0.0)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.5) {
            self.tableViewChat.contentInset = .zero
            self.constraintTextViewBottom.constant = 10
            self.view.layoutIfNeeded()
        }
        
    }
}

