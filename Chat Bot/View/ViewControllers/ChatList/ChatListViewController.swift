//
//  ChatListViewController.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 18/02/22.
//

import UIKit

protocol ChatListProtocol {
    func bindDataToView()
}

class ChatListViewController: BaseViewController {
    
    // MARK: - IBOutlets / Declarationn

    @IBOutlet weak var tableViewChatList: UITableView!
    @IBOutlet weak var labelNoData: UILabel!
    
    
    private var chatDataSource: ChatListTableViewDataSource<ChatListTableViewCell,BotModel>!
    
    private lazy var viewModel: ChatBotViewModel = {
        return ChatBotViewModel()
    }()
    
    let kIDentifier = "ChatListTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }
    
    static func loadFromNib() -> ChatListViewController {
        let vc = ChatListViewController(nibName: "ChatListViewController", bundle: nil)
        return vc
        
    }
    
    // MARK: - View Setup
    
    override func setupView() {
        super.setupView()
        
        let addChat = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(actnAddChatPressed))
        addChat.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem  = addChat
        self.title = Constants.ScreenText.kChatBot
        tableViewChatList.contentInsetAdjustmentBehavior = .never
        tableViewChatList.register(UINib(nibName: kIDentifier, bundle: nil), forCellReuseIdentifier: kIDentifier)
    }
    
    
    // MARK: - Get Data
    
    func fetchData() {
        
        viewModel.delegate = self
        viewModel.getChatData()
    }
    
    // MARK: - IB Actions/ Button action
    
    /// Will present poopup with textfield for New Bot Name. On submit will add bot in our local fille
    ///
    
    @objc override func actnAddChatPressed() {
        let alertVC = UIAlertController(title: "", message: Constants.ScreenText.kAddBotTitle, preferredStyle: .alert)
        alertVC.addTextField { (textField) in
            textField.placeholder = "Name"
        }
        
        alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alertVC] (_) in
            let textField = alertVC?.textFields![0]
            
            if let botName = textField?.text, !(botName.trimmingCharacters(in: .whitespaces) == ""), !botName.isEmpty {
                if self.viewModel.checkUniqueBotName(name: botName) {
                    let alert = UIAlertController(title: Constants.ErrrorMessage.kError, message: Constants.ErrrorMessage.kDuplicateBot, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.viewModel.addNewChatBot(name: botName)
                }
            } else {
                let alert = UIAlertController(title: Constants.ErrrorMessage.kError, message: Constants.ErrrorMessage.kInvalidBotName, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }))
        
        self.present(alertVC, animated: true, completion: nil)
    }
}


extension ChatListViewController: ChatListProtocol {
    
    // MARK: - Data Binding
    
    /// Will call it from view model to bind our DataSources. It consists closures block for TableView data source and delegate implelmentation. After initializing datasource object we will reload tableview.
    ///
    
    func bindDataToView() {
        
        self.labelNoData.isHidden = false
        self.chatDataSource = ChatListTableViewDataSource(identifier: kIDentifier, items: viewModel.chatData, configureCell: { (cell,bot) in
            self.labelNoData.isHidden = true
            cell.labelBotName.text = bot.name ?? ""
            cell.labelMessage.text = bot.lastMessage ?? ""
            cell.selectionStyle = .none
        })
        
        chatDataSource.didSelectElement  = { [unowned self] (index) in
            let view = BotChatViewController.loadFromNib()
            view.selectedIndex = index
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
            self.viewModel.selectedBotIndex = index
            
        }
        
        DispatchQueue.main.async {
            self.tableViewChatList.dataSource = self.chatDataSource
            self.tableViewChatList.delegate = self.chatDataSource
            self.tableViewChatList.reloadData()
        }
        
    }
}

extension ChatListViewController: BotMessageProtocol {
    
    // MARK: - Data Update
    
    /// Will refetch our chat data after User adds message in chat.
    ///
    func updateChatData() {
        fetchData()
    }
}
