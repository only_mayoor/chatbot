//
//  ChatDataModel.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 18/02/22.
//

import Foundation

// MARK: - Base Response Model

struct ChatBotDataModel: Codable {
    var data: [BotModel]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    init(){
        
    }
}


// MARK: - Individual Bot Model

struct BotModel: Codable {
    var name: String?
    var lastMessage: String?
    var modifiedTime: Date?
    var messageData: [MessageModel]?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case modifiedTime = "modifiedTime"
        case messageData = "messageData"
        case lastMessage = "lastMessage"
    }
    
    init(){
        
    }
}


// MARK: - Message Data Model

struct MessageModel: Codable {
    var message: String?
    var userType: String?
    var time: Date?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case userType = "userType"
        case time = "time"
    }
    init(){
        
    }
}

enum UserType:String {
    case sender
    case user
}
