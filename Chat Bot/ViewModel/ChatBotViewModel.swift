//
//  ChatBotViewModel.swift
//  Chat Bot
//
//  Created by Mayur Salvi on 18/02/22.
//

import Foundation


// We have created one common view model for both views. Ideally viewModels should be segregated but since this is small project in order to reuse some functions we will use common view model

class ChatBotViewModel {
    
    // MARK: - Setter variable
    
    /// Setter variable will set chat data .
    
    private(set) var chatData : [BotModel] = [BotModel]() {
        didSet {
            delegate?.bindDataToView()
        }
    }
    
    /// Setter variable will set Message data .
    
    private(set) var messageData : [MessageModel] = [MessageModel]() {
        didSet {
            delegateBot?.bindMessageDataToView()
        }
    }
    
    
    lazy var selectedBotIndex = 0
    
    var delegate: ChatListProtocol?
    var delegateBot: BotDataProtocol?
    
    
    // MARK: - Fetch Chat Data screen
    
    /// Fetches chat data from our local file.
    func getChatData() {
        if let responseData = ChatManager.shared.getChatData(),let data = responseData.data {
            self.chatData = data
            print("")
        }
    }
    
    /// Adds newly added Bot object to local file.
    ///
    /// - Parameters:
    ///   - name: Name of bot.
    ///
    ///   - completion: Will get chat data in calllback. We will reload data based on this chat data
    
    func addNewChatBot(name:String) {
        var newBot = BotModel()
        newBot.name = name
        newBot.modifiedTime = Date()
        newBot.messageData = [MessageModel]()
        ChatManager.shared.addBot(bot: newBot) { (response) in
            if let data = response?.data {
                chatData = data
            }
        }
    }
    
    /// Fetches chat data from our local file.
    
    func checkUniqueBotName(name:String) -> Bool {
        let botArray =  chatData.filter { $0.name == name }
        return !botArray.isEmpty
    }
    
}

extension ChatBotViewModel {
    
    // MARK: - Fetch Message Data screen
    
    
    /// Fetches message data for selected chat. Will also return name of Bot
    ///
    /// - Parameters:
    ///   - index: indexPath value of selected bot.
    ///
    func getSelectedBotData(index:Int) -> String {
        selectedBotIndex = index
        if let responseData = ChatManager.shared.getChatData(), let chatData =  responseData.data, chatData.count > index, let messageData = chatData[index].messageData  {
            self.messageData = messageData
            
            return chatData[selectedBotIndex].name ?? ""
        }
        return ""
    }
    
    /// Sends newly added message to local file.
    ///
    /// - Parameters:
    ///   - text: Message sent from chat.
    ///   - user: To identify user type.
    ///
    ///   - completion: A callback that is called when loading is finished.
    func sendMessage(text:String,user:UserType) {
        var newMessage = MessageModel()
        newMessage.message = text
        newMessage.userType = user.rawValue
        if user == .sender {
            selectedBotIndex = 0
        }
        ChatManager.shared.sendMessage(message: newMessage, index: selectedBotIndex) { (success) in
            if success {
                var newData = messageData
                newData.append(newMessage)
                messageData = newData
            }
            
        }
    }
    
}
